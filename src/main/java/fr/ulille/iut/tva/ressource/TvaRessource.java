package fr.ulille.iut.tva.ressource;

import javax.annotation.processing.Generated;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.*;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import java.util.List;
import java.util.ArrayList;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
        return TauxTva.NORMAL.taux;
    }

    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(
        @PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("{niveauTva}")
    public String getMontantTotal(@PathParam("niveauTva") String niveau, @QueryParam("somme") int somme){
        try {
            return "taux de TVA : " + getValeurTaux(niveau) + "\nsomme :" + somme + "\n";
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("lestaux")
    public List<InfoTauxDto> getInfoTaux() {
        ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
        
        for ( TauxTva t : TauxTva.values() ) {
            result.add(new InfoTauxDto(t.name(), t.taux));
        }
        
        return result;
    }

    @GET
    @Path("details/{niveauTva}")
    public String getDetails(@PathParam("niveauTva") String value, @QueryParam("somme") int somme){
        return "{'montantTotal: "+ getMontantTotal(niveau, somme) + ", montantTva: " + getValeurTaux(niveau) + ", somme: " + somme + ", tauxLabel: ";
    }
}
